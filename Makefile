SINGULARITY_CACHEDIR ?= /tmp
SINGULARITY_TMPDIR ?= /tmp

OUTPUT_DIR ?= ${CURDIR}

IMAGE_TAG ?= untagged
IMAGE_NAME = vcpipe-essentials-${IMAGE_TAG}.sif

.PHONY: help build build-and-test

help:
	@echo "USAGE:"
	@echo "make help            Display this help information."
	@echo "make build-and-test  Build a vcpipe-essentials Singularity image"
	@echo "                     and test it by displaying the installed tools."
	@echo "make clean           Remove a vcpipe-essentials Singularity image"

build-and-test:
	mkdir -p ${OUTPUT_DIR}
	${eval IMAGE_PATH := ${OUTPUT_DIR}/${IMAGE_NAME}}
	rm -f ${IMAGE_PATH}
	sudo \
	    SINGULARITY_CACHEDIR=${SINGULARITY_CACHEDIR} \
	    SINGULARITY_TMPDIR=${SINGULARITY_TMPDIR} \
	    singularity build ${IMAGE_PATH} vcpipe-essentials.def

clean:
	mkdir -p ${OUTPUT_DIR}
	${eval IMAGE_PATH := ${OUTPUT_DIR}/${IMAGE_NAME}}
	rm -f ${IMAGE_PATH}
