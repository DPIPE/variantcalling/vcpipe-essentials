# vcpipe-essentials

A project to build the `vcpipe-essentials` Singularity image giving access to most bioinformatics tools used in the pipelines developed by the HTS-diagnostic unit at OUS-AMG.

## Usage

### Build

To build the `vcpipe-essentials` image via `make`, run the following command:

`OUTPUT_DIR=<output_dir> IMAGE_TAG=<image_tag> make build-and-test`

To build the `vcpipe-essentials` image directly via `singularity build`, run the following command:

`SINGULARITY_CACHEDIR=/tmp SINGULARITY_TMPDIR=/tmp singularity build <output_dir>/<image_name>.sif vcpipe-essentials.def`

NB: should you not have root privileges in the environment where you build the `vcpipe-essentials` image, prepend `sudo` to the `singularity build` command.

### Run tools

In order to run a command with one of `vcpipe-essentials`'s tools, run for example:

`singularity exec <output_dir>/<image_name>.sif gatk --version`

If you need to use files from the host environment inside your container, use the `-B` flag to "bind" directories as follows:<br /> 
`singularity exec -B '/path/on/host/:/path/in/container/' <output_dir>/<image_name>.sif <your_command>`

If you want to run commands interactively, shell into the container using:<br /> 
`singularity shell -B '/path/on/host/:/path/in/container/' <output_dir>/<image_name>.sif`<br /> 
and run your commands.

## Update tools

In order to change the version of the tools installed in the `vcpipe-essentials` Singularity image, create a new feature branch in this repository and modify the `vcpipe-essentials.def` file accordingly.

Tip! To list available versions of a tool, run something similar to this in an existing image:
`singularity exec /singularity/vcpipe-essentials/vcpipe-essentials-2783-ggplot-d255fed9.sif conda search r-ggplot2 --channel conda-forge`

### Automatic build using `hetzner`

When pushing new commits, Gitlab's CI will automatically run a build job according to the `vcpipe-essentials.def` file from that commit and use the `hetzner` server as runner.<br />
Upon completion of such CI pipeline, the newly created `vcpipe-essentials` Singularity image will be available in `hetzner` under `/singularity/vcpipe-essentials/`.<br />
The built image will then be tagged with the project's corresponding branch/tag ID along with the commit SHA hash in short format.

### Manual cleanup of the built image

Once a new `vcpipe-essentials` Singularity image is built automatically via Gitlab's CI, as described above, it is possible to delete it manually from the server by triggering the `clean` job of the corresponding pipeline in Gitlab's CI Pipelines interface.

## Disclaimer
The MIT license is provided specifically and only for the image recipe, i.e. the definition file and not the image or container or software contained therein. Be aware of any considerations for the rights on the third-party software.
